using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using Microsoft.Azure.Devices.Common.Exceptions;
using System.Collections.Generic;

using static Xompass.Utils;
using static Newtonsoft.Json.JsonConvert;

namespace Xompass.CreateModule
{
    class ModuleInfo
    {
        [JsonProperty("deviceId"), JsonRequired]
        public string DeviceId { get; set; }
        [JsonProperty("moduleId"), JsonRequired]
        public string ModuleId { get; set; }
    }

    public static class CreateModule
    {
        [FunctionName("CreateModule")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }
            var requestBody = new StreamReader(req.Body).ReadToEnd();
            ModuleInfo moduleInfo;
            try
            {
                moduleInfo = DeserializeObject<ModuleInfo>(requestBody);
            }
            catch (JsonException ex)
            {
                return MakeErrorResponse($"Something went wrong parsing your request body: {ex.Message}");
            }
            if (moduleInfo == null)
            {
                return MakeErrorResponse($"Body must have 'deviceId' and 'moduleId' fields",
                    StatusCodes.Status400BadRequest);
            }
            try
            {
                var module = await Registry.AddModuleAsync(
                    new Module(moduleInfo.DeviceId, moduleInfo.ModuleId));
                return new JsonResult(module);
            }
            catch (ModuleAlreadyExistsException)
            {
                return MakeErrorResponse($"Module {moduleInfo.ModuleId} already exists.",
                    StatusCodes.Status400BadRequest);
            }
            catch (DeviceNotFoundException)
            {
                return MakeErrorResponse($"Device {moduleInfo.DeviceId} does not exist",
                    StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.ToString()}");
            }
        }
    }
}
