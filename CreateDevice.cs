using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;

using static Xompass.Utils;

namespace Xompass.CreateDevice
{
    public static class CreateDevice
    {
        [FunctionName("CreateDevice")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }

            var requestBody = new StreamReader(req.Body).ReadToEnd();
            var reqDevice = JsonConvert.DeserializeObject<Device>(requestBody);
            try
            {
                var createdDevice = await Registry.AddDeviceAsync(reqDevice);
                return new JsonResult(createdDevice);
            }
            catch (DeviceAlreadyExistsException)
            {
                return MakeErrorResponse($"Device {reqDevice.Id} already exists.",
                    StatusCodes.Status400BadRequest);
            }
            catch (ArgumentException)
            {
                return MakeErrorResponse($"Bad request body format",
                    StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.ToString()}");
            }
        }
    }
}
