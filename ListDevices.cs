
using System.Text;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System.Threading.Tasks;

using static Xompass.Utils;

namespace Xompass.ListDevices
{
    public static class ListDevices
    {
        [FunctionName("ListDevices")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "get", Route = null)]HttpRequest req, TraceWriter log)
        {
            // WARNING: Registry.GetDevicesAsync is obsolete! using queries.
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }

            // Get all devices
            var iotDevicesQuery = Registry.CreateQuery("SELECT * FROM devices");
            // Initialize response body (JSON)
            var responseBody = new StringBuilder();
            responseBody.Append("[");

            while (iotDevicesQuery.HasMoreResults)
            {
                foreach (var device in await iotDevicesQuery.GetNextAsJsonAsync())
                {
                    responseBody.Append(device);
                    responseBody.Append(",");
                }
            }
            if (responseBody.Length != 1)
            {
                responseBody.Remove(responseBody.Length - 1, 1); // Remove last comma
            }
            responseBody.Append("]");

            // Return response
            return new ContentResult {
                Content = responseBody.ToString(),
                ContentType = "application/json"
            };
        }
    }
}
