using System;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;

using static Xompass.Utils;

namespace Xompass.UpdateDevice
{
    public static class UpdateDevice
    {
        [FunctionName("UpdateDevice")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }

            // Explicit mode?
            bool explicitMode = req.Query["explicitMode"] == "true";

            var requestBody = new StreamReader(req.Body).ReadToEnd();
            var reqDevice = JsonConvert.DeserializeObject<Device>(requestBody);
            try
            {
                var currentDevice = await Registry.GetDeviceAsync(reqDevice.Id);
                if (currentDevice == null)
                {
                    return MakeErrorResponse($"Device {reqDevice.Id} not found",
                        StatusCodes.Status404NotFound);
                }

                currentDevice = UpdateFields(reqDevice, currentDevice, explicitMode);

                log.Info(JsonConvert.SerializeObject(reqDevice, Formatting.Indented));
                log.Info(JsonConvert.SerializeObject(currentDevice, Formatting.Indented));

                var updatedDevice = await Registry.UpdateDeviceAsync(currentDevice);
                return new JsonResult(updatedDevice);
            }
            catch (ArgumentException ex)
            {
                return MakeErrorResponse($"Bad request body format: {ex.Message}",
                    StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.ToString()}");
            }
        }

        private static Device UpdateFields(Device source, Device destination, bool explicitMode)
        {
            if (source.Authentication != null)
            {
                if (source.Authentication.SymmetricKey != null)
                {
                    if (source.Authentication.SymmetricKey.PrimaryKey != null)
                    {
                        destination.Authentication.SymmetricKey.PrimaryKey =
                            source.Authentication.SymmetricKey.PrimaryKey;
                    }
                    if (source.Authentication.SymmetricKey.SecondaryKey != null)
                    {
                        destination.Authentication.SymmetricKey.SecondaryKey =
                            source.Authentication.SymmetricKey.SecondaryKey;
                    }
                }
                if (source.Authentication.Type != 0 || explicitMode)
                {
                    destination.Authentication.Type = source.Authentication.Type;
                }
                if (source.Authentication.X509Thumbprint != null)
                {
                    if (source.Authentication.X509Thumbprint.PrimaryThumbprint != null)
                    {
                        destination.Authentication.X509Thumbprint.PrimaryThumbprint =
                            source.Authentication.X509Thumbprint.PrimaryThumbprint;
                    }
                    if (source.Authentication.X509Thumbprint.SecondaryThumbprint != null)
                    {
                        destination.Authentication.X509Thumbprint.SecondaryThumbprint =
                            source.Authentication.X509Thumbprint.SecondaryThumbprint;
                    }
                }
            }
            if (source.Capabilities != null)
            {
                destination.Capabilities = source.Capabilities;
            }
            if (source.ETag != null)
            {
                destination.ETag = source.ETag;
            }
            if (source.Status != 0 || explicitMode)
            {
                destination.Status = source.Status;
            }
            if (source.StatusReason != null)
            {
                destination.StatusReason = source.StatusReason;
            }
            return destination;
        }
    }
}
