using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.Azure.Devices.Shared;

using static Xompass.Utils;


namespace Xompass.UpdateModuleDesiredProperties
{
    public static class UpdateModuleDesiredProperties
    {
        [FunctionName("UpdateModuleDesiredProperties")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }

            string deviceId = req.Query["deviceId"];
            string moduleId = req.Query["moduleId"];

            if (deviceId == null || moduleId == null)
            {
                return MakeErrorResponse("Missing deviceId or moduleId in URL parameters",
                    StatusCodes.Status400BadRequest);
            }

            var currentTwin = await Registry.GetTwinAsync(deviceId, moduleId);
            if (currentTwin == null)
            {
                return MakeErrorResponse($"Module {deviceId}/{moduleId} not found",
                    StatusCodes.Status404NotFound);
            }

            var desiredPropertiesJson = new StreamReader(req.Body).ReadToEnd();
            var twinPatch = new Twin();
            twinPatch.Properties.Desired =
                new TwinCollection(desiredPropertiesJson);

            try
            {
                var updatedTwin = await Registry
                    .UpdateTwinAsync(deviceId, moduleId, twinPatch, currentTwin.ETag);
                return new JsonResult(updatedTwin);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.ToString()}");
            }
        }
    }
}
