using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.Azure.Devices.Shared;

using static Xompass.Utils;

namespace Xompass.ListDeviceModules
{
    public static class ListDeviceModules
    {
        [FunctionName("ListDeviceModules")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "get", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }

            string deviceId = req.Query["deviceId"];
            if (deviceId == null)
            {
                return MakeErrorResponse("Missing deviceId in URL parameters",
                    StatusCodes.Status400BadRequest);
            }

            try
            {
                var modules = await Registry.GetModulesOnDeviceAsync(deviceId);
                return new JsonResult(modules);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.ToString()}");
            }
        }
    }
}
