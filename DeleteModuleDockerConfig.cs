using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Shared;
using System.Collections.Generic;
using Xompass.Common;

using static Xompass.Utils;

namespace Xompass.DeleteModuleDockerConfig
{
    public static class DeleteModuleDockerConfig
    {
        [FunctionName("DeleteModuleDockerConfig")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "delete", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }
            string deviceId = req.Query["deviceId"];
            string moduleId = req.Query["moduleId"];
            if (deviceId == null)
            {
                return MakeErrorResponse("Missing deviceId in URL parameters",
                    StatusCodes.Status400BadRequest);
            }
            if (moduleId == null)
            {
                return MakeErrorResponse("Missing moduleId in URL parameters",
                    StatusCodes.Status400BadRequest);
            }
            try
            {
                var edgeAgent = await Registry.GetTwinAsync(deviceId, "$edgeAgent");
                TwinCollectionData data;
                if (edgeAgent != null && edgeAgent.Properties != null && edgeAgent.Properties.Desired != null)
                {
                    data = JsonConvert.DeserializeObject<TwinCollectionData>(edgeAgent
                        .Properties.Desired.ToJson());
                }
                else
                {
                    return MakeErrorResponse($"$edgeAgent module not initialized for {deviceId}",
                        StatusCodes.Status404NotFound);
                }
                if (!data.RemoveModule(moduleId))
                {
                    return MakeErrorResponse($"Module {deviceId}/{moduleId} docker config not found",
                        StatusCodes.Status404NotFound);
                }
                await Registry.ApplyConfigurationContentOnDeviceAsync(deviceId,
                    new ConfigurationContent()
                    {
                        ModuleContent = new Dictionary<string, TwinContent>()
                        {
                            {
                                "$edgeAgent",
                                new TwinContent()
                                {
                                    TargetContent = new TwinCollection(data.ToJson())
                                }
                            }
                        }
                    });
                return new StatusCodeResult(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.Message}");
            }
        }
    }
}
