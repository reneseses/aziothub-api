using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;

using static Xompass.Utils;

namespace Xompass.GetModuleDesiredProperties
{
    public static class GetModuleDesiredProperties
    {
        [FunctionName("GetModuleDesiredProperties")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "get", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }

            string deviceId = req.Query["deviceId"];
            string moduleId = req.Query["moduleId"];

            if (deviceId == null || moduleId == null)
            {
                return MakeErrorResponse("Missing deviceId or moduleId in URL parameters",
                    StatusCodes.Status400BadRequest);
            }

            try
            {
                var moduleTwin = await Registry.GetTwinAsync(deviceId, moduleId);
                if (moduleTwin == null)
                {
                    return MakeErrorResponse($"Module {deviceId}/{moduleId} not found.",
                        StatusCodes.Status404NotFound);
                }
                return new JsonResult(moduleTwin.Properties.Desired);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.ToString()}");
            }
        }
    }
}
