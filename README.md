# Azure IoT Hub API

REST API designed to interact with the internal Azure IoT Hub bindings hosted in the Azure Functions serverless architecture.

Azure functions support many languages and "triggers". In this case, all functions (endpoints) are written in C# and use the Http trigger (REST API).

> Note: to the date C# is only supported under Azure Functions `beta`; this language is used because is the most up to date with the Azure APIs.

## Deployment

Deployment of this API can be done through the [Azure portal](portal.azure.com) as a functionapp, configuring the source to be this repository. Hopefully this process can be automated with the `@xompass/azure-function-deployment` npm package.

For contextual information, refer to [https://bitbucket.org/rcmateluna/xompass-azure-function-deployment](https://bitbucket.org/rcmateluna/xompass-azure-function-deployment).

To properly setup the deployment configuration using `@xompass/azure-function-deployment`, some parameters will not be arbitrary for this particular functionapp project:

- `data.connectionString`: the connection string of the desired IoT Hub to interact with
- `data.project`: must be set to "Deployment". *Read bellow for details*
- `data.runtimeVersion`: must be set to "beta"
- `data.repoUrl`: must be set to "https://bitbucket.org/rcmateluna/aziothub-api"
- `data.branch`: must be set to "cs-migration"

> Note: at the time of writing this documentation `@xompass/azure-function-deployment` asks for a `connectionString`. In time, the azure function deployment tool will be generalized to not only fulfill the purposes of the Azure IoT Hub API.

Once the app deployment has been correctly configured, Azure will pull the **cs-migration** branch of **https://bitbucket.org/rcmateluna/aziothub-api** and serve the endpoints defined in the `.dll` binding under the **Deployment** folder right after deployment and everytime a new commit is pushed.

This means, before pushing, you must compile the app locally, copy the contents to the Deployment folder and commit. The process is shown below:

```sh
# Just finished making changes to the app...
aziothub-api$ ./publish.sh
```

> Note 1: the `publish.sh` script assumes you are in the root of the project; it's a very simple command sequence to help with deployment process.

>  Note 2: If you take a look at the script there's a line that takes care of a bug introduced by `Microsoft.NET.Sdk.Functions` which causes the generated `function.json` files to point a wrong path to the `.dll` location. More information on this can be found [here](https://github.com/Azure/azure-functions-vs-build-sdk/issues/142). Should be solved by now, but dependencies will be updated in the future.

## API definition

Endpoint specifications.

**Note on Missing Registry reference error response:**

When receiving `{ "message": "Missing Registry reference" }` it means that the `CONNECTION_STRING` environmental variable has not been set.

### Create Azure IoT Hub Device

- **URL** 

    `/api/CreateDevice`

- **Methods** 

    `POST`

- **URL Params**

    None

- **Data Params** (application/json)

    **Required:** deviceId, capabilities.iotEdge

```json
  {
      "deviceId": "<device-id:string>",
      "capabilities": {
          "iotEdge": "<is-iotedge-device?:bool>"
      }
  }
```

- **Success Response**

    - **Code:** 200

        **Content:** iot device specs

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 400

        **Content:** `{ "message": "Device <device-id> already exists" }`

    - **Code:** 400

        **Content:** `{ "message": "Bad request body format" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  POST /api/CreateDevice
  {
      "deviceId": "DemoDevice",
      "capabilities": {
          "iotEdge": true
      }
  }
```

### Create Azure IoT Hub (Edge) Device Module

- **URL** 

    `/api/CreateModule`

- **Methods** 

    `POST`

- **URL Params**

    None

- **Data Params** (application/json)

    **Required:** deviceId, moduleId

```json
  {
      "deviceId": "<device-id:string>",
      "moduleId": "<module-id:string>"
  }
```

- **Success Response**

    - **Code:** 200

        **Content:** module specs

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 500

        **Content:** `{ "message": "Something went wrong parsing your request body: <Message>" }`

    - **Code:** 400

        **Content:** `{ "message": "Body must have 'deviceId' and 'moduleId' fields" }`

    - **Code:** 400

        **Content:** `{ "message": "Module <module-id> already exists" }`

    - **Code:** 400

        **Content:** `{ "message": "Device <device-id> does not exist" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  POST /api/CreateModule
  {
      "deviceId": "DemoDevice",
      "moduleId": "DemoModule"
  }
```

### Delete Azure IoT Hub Device

- **URL** 

    `/api/DeleteDevice`

- **Methods** 

    `DELETE`

- **URL Params**

    **Required:**

    `deviceId=<deviceId>`

- **Data Params** (application/json)

    None

- **Success Response**
    - **Code:** 204

        **Content:** None

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing deviceId in URL parameters" }`

    - **Code:** 400

        **Content:** `{ "message": "Device <device-id> not found" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  DELETE /api/DeleteDevice?deviceId=DemoDevice
```

### Delete Azure IoT Hub Device Routes

- **URL** 

    `/api/DeleteDeviceRoutes`

- **Methods** 

    `DELETE`

- **URL Params**

    None

- **Data Params** (application/json)

    **Required**: deviceId, routes

```json
  {
      "deviceId": "<device-id:string>",
      "routes": ["route-ids:string",]
  }
```

- **Success Response**

    - **Code:** 200

        **Content:** `{ "routesRemoved": ["<removed-route-id:string>",] }`

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 500

        **Content:** `{ "message": "Something went wrong parsing your request body: <Message>" }`

    - **Code:** 400

        **Content:** `{ "message": "Body must have 'deviceId' and 'routes' fields" }`

    - **Code:** 500

        **Content:** `{ "message": "Device <device-id> does not exist" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  DELETE /api/DeleteDeviceRoutes
  {
      "deviceId": "DemoDevice",
      "routes": ["DemoRoute", "Route"]
  }
```

### Delete Azure IoT Hub (Edge) Device Module

- **URL** 

    `/api/DeleteModule`

- **Methods** 

    `DELETE`

- **URL Params**

    **Required:**

    `deviceId=<device-id>`

    `moduleId=<module-id>`

- **Data Params** (application/json)

    None

- **Success Response**

    - **Code:** 204

        **Content:** None

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing deviceId in URL parameters" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing moduleId in URL parameters" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  DELETE /api/DeleteModule?deviceId=DemoDevice&moduleId=DemoModule
```

### Delete Azure IoT Hub (Edge) Device Module's Docker Config

- **URL** 

    `/api/DeleteModuleDockerConfig`

- **Methods** 

    `DELETE`

- **URL Params**

    **Required:**

    `deviceId=<device-id>`

    `moduleId=<module-id>`

- **Data Params** (application/json)

    None

- **Success Response**

    - **Code:** 204

        **Content:** None

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing deviceId in URL parameters" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing moduleId in URL parameters" }`

    - **Code:** 404

        **Content:** `{ "message": "$edgeAgent module not initialized for <device-id>" }`

    - **Code:** 404

        **Content:** `{ "message": "Module <device-id>/<module-id> docker config not found" }`

    -     - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  DELETE /api/DeleteModuleDockerConfig?deviceId=DemoDevice&moduleId=DemoModule
```

### Get Azure IoT Hub Device

- **URL** 

    `/api/GetDevice`

- **Methods** 

    `GET`

- **URL Params**

    **Required:**

    `deviceId=<device-id>`

- **Data Params** (application/json)

    None

- **Success Response**

    - **Code:** 200

        **Content:** iot hub device specs

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing deviceId in URL parameters" }`

    - **Code:** 404

        **Content:** `{ "message": "Device <device-id> not found" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  GET /api/GetDevice?deviceId=DemoDevice
```

### Get Azure IoT Hub Device Connection String

- **URL** 

    `/api/GetDeviceConnectionString`

- **Methods** 

    `GET`

- **URL Params**

    **Required:**

    `deviceId=<device-id>`

- **Data Params** (application/json)

    None

- **Success Response**

    - **Code:** 200

        **Content:**

```json
    {
        "deviceId": "<device-id:string>",
        "connectionString": "<connection-string:string>"
    }
```

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing deviceId in URL parameters" }`

    - **Code:** 404

        **Content:** `{ "message": "Device <device-id> not found" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  GET /api/GetDeviceConnectionString?deviceId=DemoDevice
```

### Get Azure IoT Hub (Edge) Device Module Desired Properties

- **URL** 

    `/api/GetModuleDesiredProperties`

- **Methods** 

    `GET`

- **URL Params**

    **Required:**

    `deviceId=<device-id>`

    `moduleId=<module-id>`

- **Data Params** (application/json)

    None

- **Success Response**

    - **Code:** 200

        **Content:** module's desired properties

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing deviceId in URL parameters" }`

    - **Code:** 404

        **Content:** `{ "message": "Module <device-id>/<module-id> not found" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  GET /api/GetModuleDesiredProperties?deviceId=DemoDevice&moduleId=DemoModule
```

### List Azure IoT Hub (Edge) Device Modules

- **URL** 

    `/api/ListDeviceModules`

- **Methods** 

    `GET`

- **URL Params**

    **Required:**

    `deviceId=<device-id>`

- **Data Params** (application/json)

    None

- **Success Response**

    - **Code:** 200

        **Content:** iot edge device modules

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing deviceId in URL parameters" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  GET /api/ListDeviceModules?deviceId=DemoDevice
```

### List Azure IoT Hub Devices

- **URL** 

    `/api/ListDevices`

- **Methods** 

    `GET`

- **URL Params**

    None

- **Data Params** (application/json)

    None

- **Success Response**

    - **Code:** 200

        **Content:** list of iot hub devices

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

- **Sample Call**

```json
  GET /api/ListDevices
```

### Update Azure IoT Hub Device

- **URL** 

    `/api/UpdateDevice`

- **Methods** 

    `POST`

- **URL Params**

    `explicitMode=<explicit?>`

- **Data Params** (application/json)
  
    **Required**: deviceId

    IoT Hub device specs to be modified and their values

- **Success Response**

    - **Code:** 200

        **Content:** updated iothub device specs

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 404

        **Content:** `{ "message": "Device <device-id> not found" }`

    - **Code:** 400

        **Content:** `{ "message": "Bad request body format: <Message>" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call** (sets every config to default value)

```json
  POST /api/UpdateDevice?explicitMode=true
  {
      "deviceId": "DemoDevice"
  }
```

**Important:** When passing the iothub device specs as data params (application/json) you must make sure to set `explicitMode=true` if you want to set a particular field to it's default value. Affected fields and their respective defaults are listed below:

| Field                 | Default   |
| --------------------- | --------- |
| `authentication.type` | "sas"     |
| `status`              | "enabled" |

### Update Azure IoT Hub (Edge) Device Routes

The routes in a device define the flow of messages. Describing which sources (outputs) go to what sinks (inputs). The syntax detail can be found [here](https://docs.microsoft.com/en-us/azure/iot-edge/module-composition#specify-the-routes).

- **URL** 

    `/api/UpdateDeviceRoutes`

- **Methods** 

    `POST`

- **URL Params**

    None

- **Data Params** (application/json)

    **Required**: deviceId, edgeHubDesiredProperties.routes

```json
  {
      "deviceId": "<device-id:string>",
      "edgeHubDesiredProperties": {
          "routes": {
              "<route-id>": "<route-definition:string>",
          }
      }
  }
```

- **Success Response**

    - **Code:** 200

        **Content:** $edgeHub desired properties (includes routes definition)

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 500

        **Content:** `{ "message": "Something went wrong parsing your request body: <Message>" }`

    - **Code:** 400

        **Content:** `{ "message": "Body must have 'deviceId' field" }`

    - **Code:** 500

        **Content:** `{ "message": "Device <device-id> does not exist" }`
        
    - **Code:** 404

        **Content:** `{ "message": "Device <device-id>'s $edgeHub module not initialized" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  POST /api/UpdateDeviceRoutes
  {
      "deviceId": "DemoDevice",
      "edgeHubDesiredProperties": {
          "route": "FROM /* INTO $upstream"
      }
  }
```

### Update Azure IoT Hub (Edge) Device Module Desired Properties

- **URL** 

    `/api/UpdateModuleDesiredProperties`
    
- **Methods** 

    `POST`

- **URL Params**

    **Required:**

    `deviceId=<device-id>`

    `moduleId=<module-id>`

- **Data Params** (application/json)
  
    **Required**: None

    Desired properties arbitrary json object

- **Success Response**

    - **Code:** 200
    
        **Content:** updated twin specs

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 400

        **Content:** `{ "message": "Missing deviceId or moduleId in URL parameters" }`

    - **Code:** 404

        **Content:** `{ "message": "Module <device-id>/<moduleId> not found" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  POST /api/UpdateModuleDesiredProperties?deviceId=DemoDevice&moduleId=DemoModule
  {
      "hello": "world"
  }
```

### Update Azure IoT Hub (Edge) Device Module's Docker Config

- **URL** 

    `/api/UpdateModuleDockerConfig`
    
- **Methods** 

    `POST`

- **URL Params**

    **Required:**

    `explicitFields=<explicit-field>[,]`

    `skipCheck=<check-module-existance?:bool>`

- **Data Params** (application/json)

    **Required:** deviceId, moduleId, dockerImage

```json
  {
      "deviceId": "<device-id:string>",
      "moduleId": "<module-id:string>",
      "dockerImage": "<docker-image:string>",
      "version": "<module-version:string>",
      "restartPolicy": "<docker-container-restart-policy:string>",
      "createOptions": "<docker-container-create-options:string>"
  }
```

- **Success Response**

    - **Code:** 200
    
        **Content:** updated $edgeAgent desired properties

- **Error Response**

    - **Code:** 500

        **Content:** `{ "message": "Missing Registry reference" }`

    - **Code:** 500

        **Content:** `{ "message": "Something went wrong parsing your request body: <Message>" }`

    - **Code:** 400

        **Content:** `{ "message": "Module <module-id> does not exist" }`

    - **Code:** 404

        **Content:** `{ "message": "Body must have 'deviceId', 'moduleId' and 'dockerImage' fields" }`

    - **Code:** 500

        **Content:** `{ "message": "Unknown error: <Message>" }`

- **Sample Call**

```json
  POST /api/UpdateModuleDockerConfig?explicitFields=version,restartPolicy&skipCheck=true
  {
      "deviceId": "demoDevice",
      "moduleId": "demoModule",
      "version": "1.0",
      "restartPolicy": "always"
  }
```

**Notes on this endpoint:**

- Use `skipCheck=true` URL parameter ONLY when you are certain that the module exists. Otherwise the docker configuration will be commited to a non-existing module. Keep in mind that with `skipCheck=true` you save one roundtrip.
- Similar to updating an Azure IoT Hub device, use `explicitFields=comma,separated,fields` when you want to set a field to its default value. Affected fields and their respective defaults are listed below:

| Field           | Default  |
| --------------- | -------- |
| `version`       | "1.0"    |
| `restartPolicy` | "always" |
| `createOptions` | "{}"     |

## Local testing

To locally serve the endpoints for debugging, follow these steps:

0. Install the [Azure Functions Core Tools](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local)

1. Build the project: `./publish.sh`

2. Serve:

```sh
   aziothub-api$ cd Deployment
   aziothub-api$ func host start # this will output the endpoints
```

## Deployed API consumption

Once deployed to Azure, the endpoints are available under `https://<functionapp-name>.azurewebsites.net/`. Currently they are deployed to [https://aziothub-api.azurewebsites.net](https://aziothub-api.azurewebsites.net).

Authentication is made with a special key per-function or a master key. The latter is more comfortable to use, since is the same for each endpoint. To get the master key head to the [portal](portal.azure.com) or use the tool made for the occasion [get-azfunc-masterkey](https://github.com/rcastill/get-azfunc-masterkey).

The key can be passed as an URL parameter or in the http request headers.

**Example using URL parameter `code`**

```json
GET https://aziothub-api.azurewebsites.net/api/ListDevices?code=<master-key>
```

**Example using header `x-functions-key`**

```json
GET https://aziothub-api.azurewebsites.net/api/ListDevices
x-functions-key: <master-key>
```