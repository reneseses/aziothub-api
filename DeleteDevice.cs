using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;

using static Xompass.Utils;

namespace Xompass.DeleteDevice
{
    public static class DeleteDevice
    {
        [FunctionName("DeleteDevice")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "delete", Route = null)]HttpRequest req, TraceWriter log)
        {
            if (Registry == null)
            {
                return MakeErrorResponse("Missing Registry reference");
            }

            string deviceId = req.Query["deviceId"];
            if (deviceId == null)
            {
                return MakeErrorResponse("Missing deviceId in URL parameters",
                    StatusCodes.Status400BadRequest);
            }
            try
            {
                await Registry.RemoveDeviceAsync(deviceId);
                return new StatusCodeResult(StatusCodes.Status204NoContent);
            }
            catch (DeviceNotFoundException)
            {
                return MakeErrorResponse($"Device {deviceId} not found.",
                    StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                return MakeErrorResponse($"Unknown error: {ex.ToString()}");
            }
        }
    }
}
